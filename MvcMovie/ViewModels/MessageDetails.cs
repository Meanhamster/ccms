﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcMovie.Models
{
    public class MessageDetails
    {
        public Guid Id { get; set; }

        public Message Message { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
    }
}