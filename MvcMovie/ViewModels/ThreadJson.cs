﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcMovie.Models
{
    public class ThreadJson
    {
        public int threadID { get; set; }
        public string name { get; set; }
        public virtual ICollection<int> items { get; set; }
    }
    public class ThreadJsonArray
    {
        public virtual ICollection<ThreadJson> Threads { get; set; }
    }
}