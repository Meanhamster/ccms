﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcMovie.Models
{
    public class ContactDetails
    {
        public Guid Id { get; set; }

        public Contact Contact { get; set; }
        public virtual ICollection<Group> Groups { get; set; }
        public virtual ICollection<Message> SenderMessages { get; set; }
        public virtual ICollection<Message> RecipientMessages { get; set; }
        
    }
}