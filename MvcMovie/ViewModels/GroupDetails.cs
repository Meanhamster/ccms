﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcMovie.Models
{
    public class GroupDetails
    {
        public Guid Id { get; set; }

        public Group Group { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
    }
}