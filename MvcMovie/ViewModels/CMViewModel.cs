﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcMovie.Models
{
    public class CMViewModel
    {
        public Guid Id { get; set; }

        // SelectionBar params
        public virtual ICollection<Link> CMB_Links { get; set; }
        public int? selectedLink { get; set; }
        public int? A_Group { get; set; }
        public int? B_Group { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public DateTime Frame { get; set; }
        public Boolean Subj { get; set; }
        public Boolean Kwrd { get; set; }

        // Main screen
        public virtual ICollection<Thread> Threads { get; set; }
        public virtual ICollection<Message> A_Messages { get; set; }
        public virtual ICollection<Message> B_Messages { get; set; }
    }
}