﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using MvcMovie.Models;
using Aspose.Email.Outlook.Pst;
using Aspose.Email.Outlook;
using System.Web.Mvc;
using System.Web.Script.Serialization;

    public class FromJsonAttribute : CustomModelBinderAttribute
    {
        private readonly static JavaScriptSerializer serializer = new JavaScriptSerializer();

        public override IModelBinder GetBinder()
        {
            return new JsonModelBinder();
        }
        private class JsonModelBinder : IModelBinder
        {
            public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                var stringified = controllerContext.HttpContext.Request[bindingContext.ModelName];
                if (string.IsNullOrEmpty(stringified))
                    return null;
                return serializer.Deserialize(stringified, bindingContext.ModelType);
            }
        }
    }

    //PST reader with usage of Independentsoft dll
    public class PSTReaderIS
    {
        CCMSEntities1 db = new CCMSEntities1();
        public void ReadPSTFile(string PSTFileName)
        {
            PersonalStorage pst = PersonalStorage.FromFile(PSTFileName);
            // Get the Contacts folder
            FolderInfo folderInfo = pst.RootFolder;
            
            ParsePST(folderInfo, pst);

            pst.Dispose();
        }

        public void ParsePST(FolderInfo folderInfo, PersonalStorage pst)
        {       
            var text = folderInfo.DisplayName;
            
            MessageInfoCollection messageInfoCollection = folderInfo.GetContents();
            for (int i = 0; i < messageInfoCollection.Count; i++)
            {
                MessageInfo messageInfo = (MessageInfo) messageInfoCollection.ElementAt(i);
                MapiMessage message = pst.ExtractMessage(messageInfo);

                string _subj = message.Subject.Substring(0, (int)Math.Min(message.Subject.Length, 150));
                string _HTMLcontent = message.BodyHtml;
                string _content = message.Body;
                DateTime _datetime = message.ClientSubmitTime;
                string _senderName = message.SenderName.Substring(0, (int)Math.Min(message.SenderName.Length, 50));
                string _senderEMail = message.SenderEmailAddress.Substring(0, (int)Math.Min(message.SenderEmailAddress.Length, 50));

                int? _senderId = db.Contacts.Where(c => c.Email == _senderEMail).Select(r => r.ID).FirstOrDefault();
                if (_senderId == 0)
                {
                    MvcMovie.Models.Contact db_contact = new MvcMovie.Models.Contact { Name = _senderName, Email = _senderEMail };
                    db.Contacts.Add(db_contact);
                    db.Entry(db_contact).State = EntityState.Added;
                    db.SaveChanges();
                    _senderId = db.Contacts.Where(c => c.Email == _senderEMail).Select(r => r.ID).FirstOrDefault();
                }

                int? _messageId = db.Messages.Where(m => m.Subj == _subj && m.DateTime == _datetime).Select(r => r.ID).FirstOrDefault();
                if (_messageId == 0)
                {
                    MvcMovie.Models.Message db_message = new MvcMovie.Models.Message { Sender = _senderId, Subj = _subj, Content = _HTMLcontent, TextContent = _content, DateTime = _datetime };
                    db.Messages.Add(db_message);
                    db.Entry(db_message).State = EntityState.Added;
                    db.SaveChanges();
                    int? msg_id = db.Messages.Max(r => r.ID);
                    for (int j = 0; j < message.Recipients.Count; j++)
                    {
                        _senderName = message.Recipients[j].DisplayName.Substring(0, (int)Math.Min(message.Recipients[j].DisplayName.Length, 50));
                        _senderEMail = message.Recipients[j].EmailAddress.Substring(0, (int)Math.Min(message.Recipients[j].EmailAddress.Length, 50));

                        int? _recipientId = db.Contacts.Where(c => c.Email == _senderEMail).Select(r => r.ID).FirstOrDefault();
                        if (_recipientId == 0)
                        {
                            MvcMovie.Models.Contact db_contact = new MvcMovie.Models.Contact { Name = _senderName, Email = _senderEMail };
                            db.Contacts.Add(db_contact);
                            db.Entry(db_contact).State = EntityState.Added;
                            db.SaveChanges();
                            _recipientId = db.Contacts.Where(c => c.Email == _senderEMail).Select(r => r.ID).FirstOrDefault();                                                       
                        }
                        MvcMovie.Models.Recipient db_recipients = new MvcMovie.Models.Recipient { Messages = msg_id, Receiver = _recipientId };
                        db.Recipients.Add(db_recipients);
                        db.Entry(db_recipients).State = EntityState.Added;
                        db.SaveChanges();
                    }
                }
            }

            if (folderInfo.HasSubFolders == true)
            {
                for (int i = 0; i < folderInfo.GetSubFolders().Count; i++)
                {
                    FolderInfo subfolderInfo = (FolderInfo)folderInfo.GetSubFolders().ElementAt(i);
                    ParsePST(subfolderInfo, pst);
                }
            }
        }
    }
          /*  PstFile file = new PstFile(PSTFileName);

            using (file)
            {
                IList<Folder> folders = file.MailboxRoot.GetFolders(true);
                
                //create database with model class TestAppEntities
                CCMSEntities1 db = new CCMSEntities1();
                //random variable for ID generation
                var rand = new Random();
                
                for (int i = 0; i < folders.Count; i++)
                {
                    Folder currentFolder = folders[i];
                    IList<Item> items = currentFolder.GetItems();
                    
                    //Messages loop
                    for (int j = 0; j < items.Count; j++)
                    {
                        if (items[j] is Independentsoft.Pst.Message)
                        {
                            Independentsoft.Pst.Message pst_message = (Independentsoft.Pst.Message)items[j];

                            //read parameters from pst message
                            long pst_id = pst_message.Id;
                            string pst_subject = pst_message.Subject.Substring(0, (int)Math.Min(pst_message.Subject.Length, 50));
                            string pst_senderEMail = pst_message.SenderEmailAddress.Substring(0, (int)Math.Min(pst_message.SenderEmailAddress.Length, 50));
                            string pst_senderName = pst_message.SenderName.Substring(0, (int)Math.Min(pst_message.SenderName.Length, 50));
                            DateTime pst_creationTime = pst_message.CreationTime;
                            string pst_body = pst_message.BodyHtmlText;

                            //check for existing message in database with the same id
                            if (db.Messages.Find(pst_id) == null)
                            {
                                //create message object for db insertion
                                int? pst_senderId = db.Contacts.Where(c => c.Email == pst_senderEMail).Select(r => r.ID).FirstOrDefault();
                                
                                if(pst_senderId == 0){
                                    MvcMovie.Models.Contact db_contact = new MvcMovie.Models.Contact { Name = pst_senderName, Email =  pst_senderEMail};
                                    db.Contacts.Add(db_contact);
                                    db.Entry(db_contact).State = EntityState.Added;
                                    db.SaveChanges();
                                    pst_senderId = db.Contacts.Where(c => c.Email == pst_senderEMail).Select(r => r.ID).FirstOrDefault();
                                }
                                MvcMovie.Models.Message db_message = new MvcMovie.Models.Message { DateTime = pst_message.CreationTime, Subj = pst_subject, Sender = (int)pst_senderId, Content = pst_body };
                                db.Messages.Add(db_message);
                                db.Entry(db_message).State = EntityState.Added;
                                db.SaveChanges();
                                int? msg_id = db.Messages.Max(r => r.ID);
                                                                    
                                var cnt1 = db.Messages.Count();

                                //handling recipients
                                IList<Independentsoft.Pst.Recipient> pst_recipients = pst_message.Recipients;

                                //recipients loop
                                for (int k = 0; k < pst_recipients.Count; k++)
                                {
                                    Independentsoft.Pst.Recipient pst_recip = (Independentsoft.Pst.Recipient)pst_recipients[k];
                                    
                                    string pst_recip_name = pst_recip.DisplayName.Substring(0, (int)Math.Min(pst_recip.DisplayName.Length, 50));
                                    string pst_recip_email = pst_recip.SmtpAddress.Substring(0, (int)Math.Min(pst_recip.SmtpAddress.Length, 50));
                               
                                    //check for existing contact
                                    IList<MvcMovie.Models.Contact> exist_contact = db.Contacts.Where(e => e.Email == pst_recip_email).ToArray();
                                    
                                    //adding link to the first contact in the list with the same e-mail
                                    if (exist_contact.Count() > 0)
                                    {
                                        MvcMovie.Models.Recipient db_recipients = new MvcMovie.Models.Recipient { Messages = (int)msg_id, Receiver = exist_contact[0].ID };
                                        db.Recipients.Add(db_recipients);
                                        db.Entry(db_recipients).State = EntityState.Added;
                                        db.SaveChanges();
                                    }
                                    //creating a new contact
                                    else
                                    {
                                        MvcMovie.Models.Contact db_contact = new MvcMovie.Models.Contact { Name = pst_recip_name, Email = pst_recip_email };
                                        db.Contacts.Add(db_contact);
                                        db.Entry(db_contact).State = EntityState.Added;
                                        db.SaveChanges();
                                        int? cnt_id = db.Contacts.Max(r => r.ID);

                                        MvcMovie.Models.Recipient db_recipients = new MvcMovie.Models.Recipient { Messages = (int)msg_id, Receiver = cnt_id };
                                        db.Recipients.Add(db_recipients);
                                        db.Entry(db_recipients).State = EntityState.Added;
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                }
                db.Dispose();
            }
            */
