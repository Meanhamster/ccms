﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcMovie.Models;

namespace MvcMovie.Controllers
{
    public class RecipientsController : Controller
    {
        private CCMSEntities1 db = new CCMSEntities1();

        // GET: /Recipients/
        public ActionResult Index()
        {
            var recipients = db.Recipients.Include(r => r.Contact).Include(r => r.Message);
            return View(recipients.ToList());
        }

        // GET: /Recipients/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Recipient recipient = db.Recipients.Find(id);
            if (recipient == null)
            {
                return HttpNotFound();
            }
            return View(recipient);
        }

        // GET: /Recipients/Create
        public ActionResult Create()
        {
            ViewBag.Receiver = new SelectList(db.Contacts, "ID", "Name");
            ViewBag.Messages = new SelectList(db.Messages, "ID", "Subj");
            return View();
        }

        // POST: /Recipients/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Messages,Receiver")] Recipient recipient)
        {
            if (ModelState.IsValid)
            {
                db.Recipients.Add(recipient);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Receiver = new SelectList(db.Contacts, "ID", "Name", recipient.Receiver);
            ViewBag.Messages = new SelectList(db.Messages, "ID", "Subj", recipient.Messages);
            return View(recipient);
        }

        // GET: /Recipients/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Recipient recipient = db.Recipients.Find(id);
            if (recipient == null)
            {
                return HttpNotFound();
            }
            ViewBag.Receiver = new SelectList(db.Contacts, "ID", "Name", recipient.Receiver);
            ViewBag.Messages = new SelectList(db.Messages, "ID", "Subj", recipient.Messages);
            return View(recipient);
        }

        // POST: /Recipients/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Messages,Receiver")] Recipient recipient)
        {
            if (ModelState.IsValid)
            {
                db.Entry(recipient).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Receiver = new SelectList(db.Contacts, "ID", "Name", recipient.Receiver);
            ViewBag.Messages = new SelectList(db.Messages, "ID", "Subj", recipient.Messages);
            return View(recipient);
        }

        // GET: /Recipients/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Recipient recipient = db.Recipients.Find(id);
            if (recipient == null)
            {
                return HttpNotFound();
            }
            return View(recipient);
        }

        // POST: /Recipients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Recipient recipient = db.Recipients.Find(id);
            db.Recipients.Remove(recipient);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
