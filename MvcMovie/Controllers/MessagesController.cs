﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcMovie.Models;

namespace MvcMovie.Controllers
{
    public class MessagesController : Controller
    {
        private CCMSEntities1 db = new CCMSEntities1();

        // GET: /Messages/
        public ActionResult Index()
        {
            var messages = db.Messages.Include(m => m.Contact).Include(m => m.Thread1).Include(m => m.AGroup).Include(m => m.BGroup).Include(m => m.FromGroup);
            return View(messages.ToList());
        }

        // GET: /Messages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var md = new MessageDetails();
            md.Message = db.Messages.Find(id);
            md.Contacts = db.Recipients.Join(db.Contacts, r => r.Receiver, c => c.ID, (r, c) => new { r, c }).
                                        Where(rc => rc.r.Messages == md.Message.ID).
                                        Select(rc => rc.c).ToList();

            ViewData["myInnerHtml"] = md.Message.Content;

            if (md.Message == null)
            {
                return HttpNotFound();
            }
            return View(md);
        }

        // GET: /Messages/Create
        public ActionResult Create()
        {
            ViewBag.Sender = new SelectList(db.Contacts, "ID", "Name");
            ViewBag.Thread = new SelectList(db.Threads, "ID", "Name");
            ViewBag.A_Group = new SelectList(db.Groups, "ID", "Name");
            ViewBag.B_Group = new SelectList(db.Groups, "ID", "Name");
            ViewBag.From_Group = new SelectList(db.Groups, "ID", "Name");
            return View();
        }

        // POST: /Messages/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,DateTime,Sender,Subj,Content,Thread,A_Group,B_Group,From_Group,TextContent")] Message message)
        {
            if (ModelState.IsValid)
            {
                db.Messages.Add(message);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Sender = new SelectList(db.Contacts, "ID", "Name", message.Sender);
            ViewBag.Thread = new SelectList(db.Threads, "ID", "Name", message.Thread);
            ViewBag.A_Group = new SelectList(db.Groups, "ID", "Name", message.A_Group);
            ViewBag.B_Group = new SelectList(db.Groups, "ID", "Name", message.B_Group);
            ViewBag.From_Group = new SelectList(db.Groups, "ID", "Name", message.From_Group);
            return View(message);
        }

        // GET: /Messages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            ViewBag.Sender = new SelectList(db.Contacts, "ID", "Name", message.Sender);
            ViewBag.Thread = new SelectList(db.Threads, "ID", "Name", message.Thread);
            ViewBag.A_Group = new SelectList(db.Groups, "ID", "Name", message.A_Group);
            ViewBag.B_Group = new SelectList(db.Groups, "ID", "Name", message.B_Group);
            ViewBag.From_Group = new SelectList(db.Groups, "ID", "Name", message.From_Group);
            return View(message);
        }

        // POST: /Messages/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,DateTime,Sender,Subj,Content,Thread,A_Group,B_Group,From_Group,TextContent")] Message message)
        {
            if (ModelState.IsValid)
            {
                db.Entry(message).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Sender = new SelectList(db.Contacts, "ID", "Name", message.Sender);
            ViewBag.Thread = new SelectList(db.Threads, "ID", "Name", message.Thread);
            ViewBag.A_Group = new SelectList(db.Groups, "ID", "Name", message.A_Group);
            ViewBag.B_Group = new SelectList(db.Groups, "ID", "Name", message.B_Group);
            ViewBag.From_Group = new SelectList(db.Groups, "ID", "Name", message.From_Group);
            return View(message);
        }

        // GET: /Messages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            return View(message);
        }

        // POST: /Messages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Message message = db.Messages.Find(id);
            db.Messages.Remove(message);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
