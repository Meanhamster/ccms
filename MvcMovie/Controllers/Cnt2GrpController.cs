﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcMovie.Models;

namespace MvcMovie.Controllers
{
    public class Cnt2GrpController : Controller
    {
        private CCMSEntities1 db = new CCMSEntities1();

        // GET: /Cnt2Grp/
        public ActionResult Index()
        {
            var contacts_to_groups = db.Contacts_To_Groups.Include(c => c.Contact).Include(c => c.Group);
            return View(contacts_to_groups.ToList());
        }

        // GET: /Cnt2Grp/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contacts_To_Groups contacts_to_groups = db.Contacts_To_Groups.Find(id);
            if (contacts_to_groups == null)
            {
                return HttpNotFound();
            }
            return View(contacts_to_groups);
        }

        // GET: /Cnt2Grp/Create
        public ActionResult Create()
        {
            ViewBag.Contacts = new SelectList(db.Contacts, "ID", "Name");
            ViewBag.Groups = new SelectList(db.Groups, "ID", "Name");
            return View();
        }

        // POST: /Cnt2Grp/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Contacts,Groups")] Contacts_To_Groups contacts_to_groups)
        {
            if (ModelState.IsValid)
            {
                db.Contacts_To_Groups.Add(contacts_to_groups);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Contacts = new SelectList(db.Contacts, "ID", "Name", contacts_to_groups.Contacts);
            ViewBag.Groups = new SelectList(db.Groups, "ID", "Name", contacts_to_groups.Groups);
            return View(contacts_to_groups);
        }

        // GET: /Cnt2Grp/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contacts_To_Groups contacts_to_groups = db.Contacts_To_Groups.Find(id);
            if (contacts_to_groups == null)
            {
                return HttpNotFound();
            }
            ViewBag.Contacts = new SelectList(db.Contacts, "ID", "Name", contacts_to_groups.Contacts);
            ViewBag.Groups = new SelectList(db.Groups, "ID", "Name", contacts_to_groups.Groups);
            return View(contacts_to_groups);
        }

        // POST: /Cnt2Grp/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Contacts,Groups")] Contacts_To_Groups contacts_to_groups)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contacts_to_groups).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Contacts = new SelectList(db.Contacts, "ID", "Name", contacts_to_groups.Contacts);
            ViewBag.Groups = new SelectList(db.Groups, "ID", "Name", contacts_to_groups.Groups);
            return View(contacts_to_groups);
        }

        // GET: /Cnt2Grp/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contacts_To_Groups contacts_to_groups = db.Contacts_To_Groups.Find(id);
            if (contacts_to_groups == null)
            {
                return HttpNotFound();
            }
            return View(contacts_to_groups);
        }

        // POST: /Cnt2Grp/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Contacts_To_Groups contacts_to_groups = db.Contacts_To_Groups.Find(id);
            db.Contacts_To_Groups.Remove(contacts_to_groups);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
