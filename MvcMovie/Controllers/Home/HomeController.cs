﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using MvcMovie.Models;

namespace MvcMovie.Controllers
{
    public class HomeController : Controller
    {
        private CCMSEntities1 db = new CCMSEntities1();

        [HttpGet]
        public ActionResult Index()
        {
            var vm = new CMViewModel();
            vm.CMB_Links = db.Links.ToList();
            vm.A_Messages = db.Messages.ToList();
            vm.B_Messages = db.Messages.ToList();
            vm.Threads = db.Threads.ToList();

            return View(vm);
        }

        [HttpPost]
        public ActionResult Index(CMViewModel vm)
        {
            vm.CMB_Links = db.Links.ToList();
            int? A_Group = db.Links.Where(l => l.ID == vm.selectedLink).Select(r => r.Group_1).FirstOrDefault();
            int? B_Group = db.Links.Where(l => l.ID == vm.selectedLink).Select(r => r.Group_2).FirstOrDefault();

            vm.A_Group = A_Group;
            vm.B_Group = B_Group;

            vm.A_Messages = db.Messages.Join(db.Contacts, m => m.Sender, c => c.ID, (m, c) => new { m, c }).
                                        Join(db.Contacts_To_Groups, mc => mc.c.ID, ctg => ctg.Contacts, (mc, ctg) => new { mc, ctg }).
                                        Where(mrcc => mrcc.ctg.Groups == A_Group).
                                        Select(res => res.mc.m).ToList();
                            /*          
                                       Join(db.Recipients, mcc => mcc.mc.m.ID, r => r.Messages, (m1, r) => new { m1, r }).
                                        Join(db.Contacts, mr => mr.r.Receiver, c => c.ID, (mr, c) => new { mr, c}).
                                        Join(db.Contacts_To_Groups, mrc => mrc.c.ID, ctg1 => ctg1.Contacts, (mrc, ctg1) => new { mrc, ctg1 }).
                                        Where(mrcc => mrcc.ctg1.Groups == B_Group && mrcc.mrc.mr.m1.ctg.Groups == A_Group).
                                        Select(res => res.mrc.mr.m1.mc.m).ToList();
                            */

            vm.B_Messages = db.Messages.Join(db.Contacts, m => m.Sender, c => c.ID, (m, c) => new { m, c }).
                                    Join(db.Contacts_To_Groups, ms => ms.c.ID, ctg => ctg.Contacts, (ms, ctg) => new { ms, ctg }).
                                    Where(msg => msg.ctg.Groups == B_Group).Select(r => r.ms.m).ToList();
            //Where(m => m.Contact.Contacts_To_Groups. == vm.selectedLink).ToList();
            vm.Threads = db.Threads.Include(t => t.Messages).ToList();

            return View(vm);
        }

        [HttpPost]
        public ActionResult Init()
        {
            var jres = db.Links.Select(l => new {l.ID, l.Name}).ToList();
            return Json(jres);
        }

        [HttpPost]
        public ActionResult SaveThreads(string Threads, int? link)
        {
            var jThreads = Threads.Replace("\\", "");
            jThreads = jThreads.Substring(1, jThreads.Length - 2);
            var jstring = "{\"Threads\":[{\"threadID\":-1,\"items\":[]},{\"threadID\":-2,\"items\":[]},{\"threadID\":1002,\"items\":[2231]}]}";

            var arr = JsonConvert.DeserializeObject<ThreadJsonArray>(jThreads);
            foreach (ThreadJson Thread in arr.Threads)
            {
                var dbID = 0;
                if(Thread.threadID < 0)
                {
                    MvcMovie.Models.Thread db_thread = new MvcMovie.Models.Thread { Name = Thread.name };
                    db.Threads.Add(db_thread);
                    db.Entry(db_thread).State = EntityState.Added;
                    db.SaveChanges();
                    Thread.threadID = db.Threads.Max(r => r.ID);
                }
                db.Database.ExecuteSqlCommand("UPDATE Messages SET Thread = null Where Thread = {0}", Thread.threadID);
                foreach (int ItemID in Thread.items)
                {
                    db.Database.ExecuteSqlCommand("UPDATE Messages SET Thread = {0} Where ID = {1}", Thread.threadID, ItemID);
                }
                
            }
            //delete empty threads

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult ReadMessagesB(int? _link, DateTime? _begDate, DateTime? _endDate)
        {
            var vm = new CMViewModel();
          //  var begDate = (_begDate == null) ? (DateTime)("01-01-1900".tod) : _begDate;
            var begDate = _begDate;
            var endDate = _endDate;
            int? A_Group = db.Links.Where(l => l.ID == _link).Select(r => r.Group_1).FirstOrDefault();
            int? B_Group = db.Links.Where(l => l.ID == _link).Select(r => r.Group_2).FirstOrDefault();

            var FromB = db.Messages.Join(db.Contacts, m => m.Sender, c => c.ID, (m, c) => new { m, c }).
                                   Join(db.Contacts_To_Groups, mc => mc.c.ID, ctg => ctg.Contacts, (mc, ctg) => new { mc, ctg }).
                                  Where(res => res.ctg.Groups == B_Group && res.mc.m.DateTime >= begDate && res.mc.m.DateTime <= endDate).
                                 Select(res => res.mc.m).ToList();

            var ToA = db.Messages.Join(db.Recipients, m => m.ID, r => r.Messages, (m, r) => new { m, r }).
                                     Join(db.Contacts_To_Groups, mr => mr.r.Receiver, ctg => ctg.Contacts, (mr, ctg) => new { mr, ctg }).
                                    Where(res => res.ctg.Groups == A_Group).
                                   Select(res => res.mr.m).ToList();
            var jres = FromB.Intersect(ToA).Select(res => new {Type = "b", res.ID, ThreadID = (res.Thread1 == null) ? 0 : res.Thread1.ID, ThreadName = (res.Thread1 == null) ? "" : res.Thread1.Name, res.DateTime, res.Contact.Name, res.Subj, res.TextContent, res.Content }).ToList();

            var FromA = db.Messages.Join(db.Contacts, m => m.Sender, c => c.ID, (m, c) => new { m, c }).
                                     Join(db.Contacts_To_Groups, mc => mc.c.ID, ctg => ctg.Contacts, (mc, ctg) => new { mc, ctg }).
                                    Where(res => res.ctg.Groups == A_Group && res.mc.m.DateTime >= begDate && res.mc.m.DateTime <= endDate).
                                   Select(res => res.mc.m).ToList();

            var ToB = db.Messages.Join(db.Recipients, m => m.ID, r => r.Messages, (m, r) => new { m, r }).
                                     Join(db.Contacts_To_Groups, mr => mr.r.Receiver, ctg => ctg.Contacts, (mr, ctg) => new { mr, ctg }).
                                    Where(res => res.ctg.Groups == B_Group).
                                   Select(res => res.mr.m).ToList();

            jres = jres.Union(FromA.Intersect(ToB).Select(res => new { Type = "a", res.ID, ThreadID = (res.Thread1 == null) ? 0 : res.Thread1.ID, ThreadName = (res.Thread1 == null) ? "" : res.Thread1.Name, res.DateTime, res.Contact.Name, res.Subj, res.TextContent, res.Content })).ToList();

            return Json(jres);
        }
        public ActionResult LeftScreen()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}