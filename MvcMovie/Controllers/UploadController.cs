﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MvcMovie.Models;

namespace MvcMovie.Controllers
{
    public class UploadController : Controller
    {
        private CCMSEntities1 db = new CCMSEntities1();
        //
        // GET: /Upload/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UploadFile()
        {
            foreach (string inputTagName in Request.Files)
            {
                HttpPostedFileBase file = Request.Files[inputTagName];
                if (file.ContentLength > 0)
                {
                    string filePath = Path.Combine(HttpContext.Server.MapPath("../Uploads"), Path.GetFileName(file.FileName));
                    file.SaveAs(filePath);
                    PSTReaderIS reader = new PSTReaderIS();
                    reader.ReadPSTFile(filePath);
                }
            }
            return RedirectToAction("Index");
        }
        public ActionResult ClearAll()
        {
            db.Recipients.RemoveRange(db.Recipients.ToArray());
            db.Contacts_To_Groups.RemoveRange(db.Contacts_To_Groups.ToArray());
            db.Messages.RemoveRange(db.Messages.ToArray());
            db.Contacts.RemoveRange(db.Contacts.ToArray());
            db.Threads.RemoveRange(db.Threads.ToArray());
            db.ThreadTypes.RemoveRange(db.ThreadTypes.ToArray());
            db.Links.RemoveRange(db.Links.ToArray());
            db.Groups.RemoveRange(db.Groups.ToArray());
            db.SaveChanges();
            return RedirectToAction("Index");
        }
	}
}