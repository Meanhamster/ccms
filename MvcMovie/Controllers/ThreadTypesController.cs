﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcMovie.Models;

namespace MvcMovie.Controllers
{
    public class ThreadTypesController : Controller
    {
        private CCMSEntities1 db = new CCMSEntities1();

        // GET: /ThreadTypes/
        public ActionResult Index()
        {
            return View(db.ThreadTypes.ToList());
        }

        // GET: /ThreadTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ThreadType threadtype = db.ThreadTypes.Find(id);
            if (threadtype == null)
            {
                return HttpNotFound();
            }
            return View(threadtype);
        }

        // GET: /ThreadTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /ThreadTypes/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Name,DateTime")] ThreadType threadtype)
        {
            if (ModelState.IsValid)
            {
                db.ThreadTypes.Add(threadtype);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(threadtype);
        }

        // GET: /ThreadTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ThreadType threadtype = db.ThreadTypes.Find(id);
            if (threadtype == null)
            {
                return HttpNotFound();
            }
            return View(threadtype);
        }

        // POST: /ThreadTypes/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Name,DateTime")] ThreadType threadtype)
        {
            if (ModelState.IsValid)
            {
                db.Entry(threadtype).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(threadtype);
        }

        // GET: /ThreadTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ThreadType threadtype = db.ThreadTypes.Find(id);
            if (threadtype == null)
            {
                return HttpNotFound();
            }
            return View(threadtype);
        }

        // POST: /ThreadTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ThreadType threadtype = db.ThreadTypes.Find(id);
            db.ThreadTypes.Remove(threadtype);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
