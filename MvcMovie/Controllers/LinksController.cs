﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcMovie.Models;

namespace MvcMovie.Controllers
{
    public class LinksController : Controller
    {
        private CCMSEntities1 db = new CCMSEntities1();

        // GET: /Links/
        public ActionResult Index()
        {
            var links = db.Links.Include(l => l.Group).Include(l => l.Group1);
            return View(links.ToList());
        }

        // GET: /Links/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Link link = db.Links.Find(id);
            if (link == null)
            {
                return HttpNotFound();
            }
            return View(link);
        }

        // GET: /Links/Create
        public ActionResult Create()
        {
            ViewBag.Group_1 = new SelectList(db.Groups, "ID", "Name");
            ViewBag.Group_2 = new SelectList(db.Groups, "ID", "Name");
            return View();
        }

        // POST: /Links/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Name,Group_1,Group_2")] Link link)
        {
            if (ModelState.IsValid)
            {
                db.Links.Add(link);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Group_1 = new SelectList(db.Groups, "ID", "Name", link.Group_1);
            ViewBag.Group_2 = new SelectList(db.Groups, "ID", "Name", link.Group_2);
            return View(link);
        }

        // GET: /Links/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Link link = db.Links.Find(id);
            if (link == null)
            {
                return HttpNotFound();
            }
            ViewBag.Group_1 = new SelectList(db.Groups, "ID", "Name", link.Group_1);
            ViewBag.Group_2 = new SelectList(db.Groups, "ID", "Name", link.Group_2);
            return View(link);
        }

        // POST: /Links/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Name,Group_1,Group_2")] Link link)
        {
            if (ModelState.IsValid)
            {
                db.Entry(link).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Group_1 = new SelectList(db.Groups, "ID", "Name", link.Group_1);
            ViewBag.Group_2 = new SelectList(db.Groups, "ID", "Name", link.Group_2);
            return View(link);
        }

        // GET: /Links/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Link link = db.Links.Find(id);
            if (link == null)
            {
                return HttpNotFound();
            }
            return View(link);
        }

        // POST: /Links/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Link link = db.Links.Find(id);
            db.Links.Remove(link);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
